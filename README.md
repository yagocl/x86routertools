# x86routertools

Scripts for using any Linux PC with a capable WiFi NIC as a fully-fledged wireless access point.

It can check and automatically start up hostapd with your custom configurations, making it very useful in cron jobs (as a watchdog).     
`* * * * * routertools check-wifi`     <-- checks and restarts AP if interface down or hostapd down       
`* * * * * routertools check-inet`     <-- checks and restarts internet if its interface is down

Along with checking if the network is live, it can also be configured to send a warning to TTY or X11 desktop about the problem. Though, it will attempt to solve the issue on its own if properly set up. (by restarting things, it can't fix the likes of a snapped cable)  

It can check and automatically set up Cake SQM (+ BBR) on new network interfaces, to pretty much nullify bufferbloat. It does this best as a crontab watchdog, and categorizes network interfaces in profiles, some configurable with traffic shaping. This is similar to what sqm-scripts does, but the code is different.     
`* * * * * routertools qdisc`    

It automates tedious tasks such as setting up NAT, ipv6 forwarding, ath9k paprd, ath9k lna mixing, tx power setting, dynack, regdomain setting, ... and service restarting that cannot be done within hostapd.     
`sudo routertools start-wifi`     

Configuration is done via text file editing, you can find every relevant option in folder /etc/routertools.d/     
Any form of internet access is supported via scripts in routertools.d, but Arch Linux's rp-pppoe scripts are defaults.      

Any amount of AP interfaces, hostapd instances and virtual bssids is supported, via profiles.       
A valid profile has two files:         

`/etc/routertools.d/wifi-access-points/[interface_name].conf`  <-- standard x86routertools interface config file          
`/etc/routertools.d/wifi-access-points/[interface_name]_hostapd.conf` <-- your custom hostapd.conf    

x86routertools can be installed by downloading the git repository and copying `routertools` to /usr/bin:     
`sudo wget https://gitlab.com/yagocl/x86routertools/-/raw/master/routertools -O /usr/bin/routertools; sudo chmod og+xr /usr/bin/routertools`    
Then generating the base configuration files:        
`sudo routertools reset-cfg`     
If you want to be able to call it with the fancier command 'rt':     
`sudo ln -s /usr/bin/routertools /usr/bin/rt`     
```
```

System Requirements:      
`A wifi and AP capable NIC, preferably one that uses the ath9k driver`     
`Any systemd-based distribution`        
`netstat`        
`iw`     
`ifconfig`           
`hostapd`
```
```
x86routertools is free software licensed under the GPLv3.         
Copyright (C) 2020 Yago Mont' Alverne         
